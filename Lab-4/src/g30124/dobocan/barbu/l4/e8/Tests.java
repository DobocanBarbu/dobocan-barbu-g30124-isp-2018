package g30124.dobocan.barbu.l4.e8;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
public class Tests {
@Test
public void t01()
{
	Shape q = new Shape();
	q.setColor("blue");
	//q.setFilled(false);
	assertEquals(q.getColor(),"blue",0.01);
	//assertEquals(q.isFilled(),false,0.01);
}
	
@Test
public void t11()
{
	Circle q = new Circle(5);
	assertEquals(q.getArea(),3.14*5*5,0.01);
}

@Test
public void t12()
{
	Circle q = new Circle(5);
	assertEquals(q.getPerimeter(),2*3.14*5,0.01);
}

@Test
public void t13()
{
	Circle q = new Circle();
	q.setRadius(5);
	assertEquals(q.getRadius(),5,0.01);
}

@Test
public void t21()
{
	Rectangle q = new Rectangle(5,4);
	assertEquals(q.getArea(),5*4,0.01);
}

@Test
public void t22()
{
	Rectangle q = new Rectangle(5,4);
	assertEquals(q.getPerimetre(),2*(5+4),0.01);
}

@Test
public void t23()
{
	Rectangle q = new Rectangle();
	q.setWidth(5);
	q.setLength(5);
	assertEquals(q.getWidth(),5,0.01);
	assertEquals(q.getLength(),5,0.01);
}


@Test
public void t31()
{
	Square q = new Square(5);
	assertEquals(q.getSide(),5,0.01);
}

@Test
public void t32()
{
	Square q = new Square();
	q.setSide(6);
	assertEquals(q.getSide(),6,0.01);
}

}
