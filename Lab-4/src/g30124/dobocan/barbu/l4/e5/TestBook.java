package g30124.dobocan.barbu.l4.e5;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import g30124.dobocan.barbu.l4.e4.Author;
public class TestBook {
@Test 
public void shouldBeSameAuthor()
{
	Author a = new Author("Cargiale","ILCaragiale@gmail.com",'m');
	Book b = new Book("Baltagul", a ,20);
	assertEquals(a.getName(),b.getAuthor().getName(),0.01);
}
@Test 
public void shouldChangeTheQTY()
{
	Author a = new Author("Cargiale","ILCaragiale@gmail.com",'m');
	Book b = new Book("Baltagul", a ,20,100);
	b.setQytInStock(99);
	assertEquals(b.getQytInStock(),99,0.01);
}

}
