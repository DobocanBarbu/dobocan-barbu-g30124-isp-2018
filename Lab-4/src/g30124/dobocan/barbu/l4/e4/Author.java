package g30124.dobocan.barbu.l4.e4;

public class Author {
	String name;
	String email;
	 char g;
	public Author(String name,String email,char g)
	{
		this.name = name;
		this.email=email;
		this.g=g;
	}
	
	void setEmail(String email)
	{
		this.email = email;
	}
	
	public String getName()
	{
		return name;
	}
	
	String getEmail()
	{
		return email;
	}
	
	char getGender()
	{
		return g;
	}
	
	public String toString()
	{
		return this.name + " (" + this.g +") at " + this.email;
	}
	
	public static void main(String[] args)
	{
		Author a = new Author("Eminescu","meminescu@gmail.com",'m');
		Author b = new Author("Cargiale","ILCaragiale@gmail.com",'m');
		System.out.println(a.getName() + " " + a.getEmail());
		System.out.println(a.toString());
	}
}
