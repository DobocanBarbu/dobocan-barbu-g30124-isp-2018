package g30124.dobocan.barbu.l4.e4;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestAuthor {
	@Test
public void shouldCreateaName()
{
	Author a = new Author("Eminescu","meminescu@gmail.com",'m');
	 assertEquals(a.getName(), "Eminescu",0.01);	
}
	@Test
public void shouldCreateaEmail()
{
	Author a = new Author("Eminescu","meminescu@gmail.com",'m');
	assertEquals(a.getEmail(), "meminescu@gmail.com",0.01);	
}
	@Test
public void shouldChangeaEmail()
{
	Author a = new Author("Eminescu","meminescu@gmail.com",'m');
	a.setEmail("MihaiEminescu@yahoo.com");
	assertEquals(a.getEmail(), "MihaiEminescu@yahoo.com",0.01);	
}
	@Test
public void shouldCreateaGender()
{
	Author a = new Author("Eminescu","meminescu@gmail.com",'m');
	assertEquals(a.getGender(), 'm',0.01);	
}
	@Test
public void shouldShowToString()
{
	Author a = new Author("Eminescu","meminescu@gmail.com",'m');
	assertEquals(a.toString(), "Eminescu (m) at meminescu@gmail.com ",0.01);	
}

}
