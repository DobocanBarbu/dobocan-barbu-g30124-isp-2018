package g30124.dobocan.barbu.l4.e6;

import g30124.dobocan.barbu.l4.e4.Author;


public class Book {
	String name;
	Author a[]=new Author[100];
	int price;
	int qtyInStock;
	public Book(String name,Author a[],int price)
	{
		this.name = name;
		this.a = a;
		this.price = price;
	}
	
	public Book(String name,Author a[],int price,int qtyInStock)
	{
		this.name = name;
		this.a = a;
		this.price = price;
		this.qtyInStock = qtyInStock;
	}
	

	public String getName()
	{
		return this.name;
	}
	
	public Author[] getAuthor()
	 {
		 return a;
	 }
	
	public int getPrice()
	 {
		return this.price; 
	 }
	 
	public void setPrice(int price)
	 {
		 this.price = price;
	 }
	 
	public void setQytInStock(int qis)
	 {
		this.qtyInStock = qis;
	 }
	
	public  int getQytInStock()
	 {
		 return this.qtyInStock;
	 }

	public String toString()
	{
		int i=0;
		while(a[i] != null)
		{
			i++;
		}
		i++;
		return name + " by " + i + " authors";
	}
	
	public void printAuthors()
	{
		int i=0;
		while(a[i] != null)
		{
			System.out.println(a[i].getName() + " ");
			i++;
		}
	}
	
	public static void main(String[] args)
	{
		Author a0 = new Author("Cargiale","ILCaragiale@gmail.com",'m');
		Author a1 = new Author("Cargiale1","ILCaragiale@gmail.com",'m');
		Author a2= new Author("Cargiale2","ILCaragiale@gmail.com",'m');
		Author a3 = new Author("Cargiale3","ILCaragiale@gmail.com",'m');
		Author a4 = new Author("Cargiale14","ILCaragiale@gmail.com",'m');
		Author[] a= {a0,a1,a2,a3,a4};
		Book b = new Book("Baltagul", a ,20);
		//b.printAuthors();
		System.out.println(b.toString());
	}
	
}
