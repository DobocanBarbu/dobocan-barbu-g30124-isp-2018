package g30124.dobocan.barbu.l4.e6;
//import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.junit.Test;

import g30124.dobocan.barbu.l4.e4.Author;
public class TestBook {

	@Test
	public void shouldHaveAuthors()
	{
		Author a0 = new Author("Cargiale","ILCaragiale@gmail.com",'m');
		Author a1 = new Author("Cargiale1","ILCaragiale@gmail.com",'m');
		Author a2= new Author("Cargiale2","ILCaragiale@gmail.com",'m');
		Author a3 = new Author("Cargiale3","ILCaragiale@gmail.com",'m');
		Author a4 = new Author("Cargiale14","ILCaragiale@gmail.com",'m');
		Author[] a= {a0,a1,a2,a3,a4};
		Book b = new Book("Baltagul", a ,20);
		assertEquals(b.getAuthor(),a,0.01);
	}
	
	private void assertEquals(Author[] author, Author[] a, double d) {
		// TODO Auto-generated method stub	
	}
	//de ce a fost nevoie sa creez functia asta?
	
	
	@Test
	public void shouldChangePrice()
	{
		Author a0 = new Author("Cargiale","ILCaragiale@gmail.com",'m');
		Author[] a= {a0};
		Book b = new Book("Baltagul", a ,20,100);
		b.setPrice(30);
		Assert.assertEquals(b.getPrice(),30,0.01);
	}
	
	@Test
	public void shouldChangeQIT()
	{
		Author a0 = new Author("Cargiale","ILCaragiale@gmail.com",'m');
		Author[] a= {a0};
		Book b = new Book("Baltagul", a ,20,100);
		b.setQytInStock(30);
		Assert.assertEquals(b.getQytInStock(),30,0.01);
	}
	
	@Test
	public void shouldHaveNAuthors()
	{
		Author a0 = new Author("Cargiale","ILCaragiale@gmail.com",'m');
		Author a1 = new Author("Cargiale1","ILCaragiale@gmail.com",'m');
		Author a2= new Author("Cargiale2","ILCaragiale@gmail.com",'m');
		Author a3 = new Author("Cargiale3","ILCaragiale@gmail.com",'m');
		Author a4 = new Author("Cargiale14","ILCaragiale@gmail.com",'m');
		Author[] a= {a0,a1,a2,a3,a4};
		Book b = new Book("Baltagul", a ,20);
		String x = (b.getName()+" by " + 4 + " authors");
		assertEquals(b.toString(),x,0.01);
	}

	private void assertEquals(String string, String string2, double d) {
		// TODO Auto-generated method stub
		
	}
	
	/*
	@Test
	public void shouldPrintAuthors()
	{
		Author a0 = new Author("Cargiale","ILCaragiale@gmail.com",'m');
		Author a1 = new Author("Cargiale1","ILCaragiale@gmail.com",'m');
		Author a2= new Author("Cargiale2","ILCaragiale@gmail.com",'m');
		Author a3 = new Author("Cargiale3","ILCaragiale@gmail.com",'m');
		Author a4 = new Author("Cargiale14","ILCaragiale@gmail.com",'m');
		Author[] a= {a0,a1,a2,a3,a4};
		Book b = new Book("Baltagul", a ,20);
		assertEquals(b.printAuthors(),b.getAuthor(),0.01);
	}
	*/
}
