package g30124.dobocan.barbu.l6.e3;

import java.awt.Color;
import java.awt.Graphics;

public interface Shape {


    
    public int getX();

    
    public int getY();

    
    public String getId();

    
    public boolean isFill();


    public void Shape();


    public Color getColor() ;

    public abstract void draw(Graphics g);
}
