package g30124.dobocan.barbu.l8.e1;

public class TooManyCoffesException extends Exception{
    int nrCoffee;
    public TooManyCoffesException(int nrCoffee,String msg) {
        super(msg);
        this.nrCoffee = nrCoffee;
    }

    int getNrCoffee(){
        return nrCoffee;
    }
}//.class
