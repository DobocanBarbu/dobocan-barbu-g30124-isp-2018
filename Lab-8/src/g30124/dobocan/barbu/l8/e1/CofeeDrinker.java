package g30124.dobocan.barbu.l8.e1;

public class CofeeDrinker {
	void drinkCofee(Cofee c) throws TemperatureException, ConcentrationException, TooManyCoffesException{
        if(c.getNrCoffee()>10)
            throw new TooManyCoffesException(c.getNrCoffee(),"Too many coffes!");
        if(c.getTemp()>60)
              throw new TemperatureException(c.getTemp(),"Cofee is to hot!");
        if(c.getConc()>50)
              throw new ConcentrationException(c.getConc(),"Cofee concentration to high!");
        System.out.println("Drink cofee:"+c);
  }
}
