package g30124.dobocan.barbu.l8.e1;

public class Cofee {
	private int temp;
    private int conc;
    private int nrCoffee;
    Cofee(int t,int c,int nC){temp = t;conc = c;nrCoffee = nC;}
    int getTemp(){return temp;}
    int getConc(){return conc;}
    int getNrCoffee(){return nrCoffee;}
    public String toString(){return "[cofee temperature="+temp+":concentration="+conc+":Coffee Number="+ nrCoffee +"]";}
}
