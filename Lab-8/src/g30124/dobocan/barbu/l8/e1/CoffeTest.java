package g30124.dobocan.barbu.l8.e1;

public class CoffeTest {
	public static void main(String[] args) throws TooManyCoffesException, TemperatureException, ConcentrationException {
        CofeeMaker mk = new CofeeMaker();
        CofeeDrinker d = new CofeeDrinker();

        for(int i = 0;i<15;i++){
              Cofee c = mk.makeCofee();
              try {
                    d.drinkCofee(c);
              }catch (TooManyCoffesException e) {
                    System.out.println("Exception:"+e.getMessage()+" nrCoffee="+e.getNrCoffee());
                    i=15;
              } catch (TemperatureException e) {
                    System.out.println("Exception:"+e.getMessage()+" temp="+e.getTemp());
              } catch (ConcentrationException e) {
                    System.out.println("Exception:"+e.getMessage()+" conc="+e.getConc());
              }
              finally{
                    System.out.println("Throw the cofee cup.\n");
              }
        }    
  }
}
