package g30124.dobocan.barbu.l8.e33;

//package g30124.dobocan.barbu.l8.e3;
import java.util.Scanner;
import java.io.*;

public class Main {
    public static void Cript(String name) throws IOException {
        BufferedReader a = new BufferedReader(new FileReader(name));
        BufferedWriter b = new BufferedWriter(new FileWriter("exercise3.eng.txt"));
        String line;
        while((line=a.readLine()) != null)
        {
            int n = line.length();

            for(int i=0;i<n;i++)
            {
                char c=line.charAt(i);
                c++;
                b.write(c);
            }
            b.newLine();
        }
        b.close();
    }

    public static void Descript(String name) throws IOException
    {
        BufferedReader a = new BufferedReader(new FileReader(name));
        BufferedWriter b = new BufferedWriter(new FileWriter("exercise3.deg.txt"));
        String line;
        while((line=a.readLine()) != null)
        {
            int n = line.length();

            for(int i=0;i<n;i++)
            {
                char c=line.charAt(i);
                c--;
                b.write(c);
            }
            b.newLine();
        }
        b.close();
    }


    public static void main(String[] args) throws IOException {
        
        Scanner in = new Scanner(System.in);
        String file = in.nextLine();
        Descript(file);
        Cript(file);
    }
}

