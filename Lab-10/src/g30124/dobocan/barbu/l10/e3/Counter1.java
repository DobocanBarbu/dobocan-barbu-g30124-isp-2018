package g30124.dobocan.barbu.l10.e3;

public class Counter1 extends Thread{

    Thread t;

    Counter1(String name,Thread t)
    {
        super(name);
        this.t=t;
    }

    public void run()
    {

        for(int i=1;i<=100;i++)
        {
            System.out.println(getName() + " i=" + i);
            try{
                if(t!=null) t.join();
                Thread.sleep((int)(Math.random() * 100));
            }
            catch(InterruptedException e)
            {
                e.printStackTrace();
            }

        }
        System.out.println(getName() + " job dinalised");
    }

}

