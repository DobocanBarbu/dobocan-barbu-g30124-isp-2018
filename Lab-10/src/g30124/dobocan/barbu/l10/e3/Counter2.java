package g30124.dobocan.barbu.l10.e3;

public class Counter2 extends Thread{

    Thread t;

    Counter2(String name,Thread t)
    {
        super(name);
        this.t=t;
    }

    public void run()
    {
        for(int i=101;i<=200;i++)
        {
            System.out.println(getName() + " i=" + i);
            try{
                if(t!=null) t.join();
                Thread.sleep((int)(Math.random() * 100));
            }
            catch(InterruptedException e)
            {
                e.printStackTrace();
            }

        }
        System.out.println(getName() + " job dinalised");
    }

}
