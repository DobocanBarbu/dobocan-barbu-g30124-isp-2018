package g30124.dobocan.barbu.l10.e4;

public class Robot extends Thread {
   private int x;
   private int y;
   private int maxXY=100;
   private int minXY=0;
   private String name;
   boolean alive;

   public Robot(String name)
   {
       super(name);
       this.name=name;
       this.x=(int) (Math.random() * 100);
       this.y=(int) (Math.random() * 100);
       this.alive=true;
   }



   public void setX(int x)
   {
       this.x=x;
   }

   public void setY(int y)
   {
       this.y=y;
   }


   public int getX()
   {
       return this.x;
   }

   public int getY()
   {
       return this.y;
   }

   public void isAlive(boolean alive)
   {
       this.alive=alive;
   }

   public void move()
   {
        int step;
        do{
            step = (int) (Math.random() * 100);
           // System.out.println("PASUL" + step);

            if (step % 4 == 0)
                //a.setX(a.getX() + 1);
            this.x++;
            else if (step % 4 == 1)
               //a.setX(a.getX() - 1);
                this.x--;
            else if (step % 4 == 2)
               //a.setY(a.getY() + 1);
                this.y++;
            else if (step % 4 == 3)
                //a.setY(a.getY() - 1);
                this.y--;
        }while(this.x<=minXY && this.y<=minXY && this.x>=maxXY && this.y>=maxXY);
   }

   public void destroy(Robot a)
   {
       a.setX(-1);
       a.setY(-1);
       a.isAlive(false);
       a.interrupt();

   }

    public void run()
    {
        while(true)
        {

            try {
                move();
                System.out.println(this.name+"  este pe pozitia X="+this.getX()+"  Y="+this.getY());
                Thread.sleep((int)(2000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
