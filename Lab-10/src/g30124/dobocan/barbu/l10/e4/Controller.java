package g30124.dobocan.barbu.l10.e4;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.TreeSet;

import static java.lang.Thread.sleep;

public class Controller
{
    ArrayList<Robot> robots;

    public Controller()
    {
        robots=new ArrayList<Robot>();
        startSimulation();
    }

    private void startSimulation()
    {
        for(int i=0;i<10;i++)
        {
            robots.add(new Robot("Robot"+i));
            robots.get(i).start();
        }

        TreeSet<Robot> toBeRemoved;

        while(robots.size()!=0) {
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            toBeRemoved = new TreeSet<>(new Comparator<Robot>() {
                @Override
                public int compare(Robot o1, Robot o2) {
                    if(o1.getName() == o2.getName())
                        return 0;
                    else
                        return -1;
                }
            });
            for(int i=0;i<robots.size();i++)
            {
                for(int j=i+1;j<robots.size();j++)
                {
                    if(robots.get(i).getX()==robots.get(j).getX() && robots.get(i).getY() == robots.get(j).getY()) {
                        toBeRemoved.add(robots.get(j));
                        toBeRemoved.add(robots.get(i));
                        robots.get(i).isAlive(false);
                        robots.get(j).isAlive(false);
                    }
                }
            }
            robots.removeAll(toBeRemoved);


        }
    }
}

