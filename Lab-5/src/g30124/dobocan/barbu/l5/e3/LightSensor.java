package g30124.dobocan.barbu.l5.e3;
import java.util.Random;

public class LightSensor extends Sensor{
	Random rand = new Random();
	int light = rand.nextInt();
	@Override
	public int ReadValue() {
		if(light>0)
		return light%100;
		return light%100*(-1);
	}

}
