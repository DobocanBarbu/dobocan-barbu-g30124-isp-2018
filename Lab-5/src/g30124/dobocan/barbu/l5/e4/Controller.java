package g30124.dobocan.barbu.l5.e4;
import java.util.Timer;
import java.util.TimerTask;


public class Controller {
	 
    private static Controller a;
// Prevent clients from using the constructor
    private Controller() {
    	control();
    }
 
//Control the accessible (allowed) instances
    public static Controller getController() {
        if (a == null) {
            a = new Controller();
        }
        
        return a;
    }
    
    public static void control()
    {

    	Timer myTimer = new Timer();
    	TimerTask task = new TimerTask() {

    		public void run() {
       		 int secondPassed = 0;
    			TemperatureSensor ts = new TemperatureSensor();
    			LightSensor ls = new LightSensor();
    			secondPassed++;
    			System.out.println("Sec("+secondPassed+") Temperature="+ts.ReadValue()+"  Light="+ls.ReadValue());   
    			if (secondPassed == 20)
    		        myTimer.cancel();
    		}
    	};
    	myTimer.scheduleAtFixedRate(task,1000,1000);
    }
 
    
    public static void main(String[] args)
    {
    	//Controller a; 
    	getController();
    }
    
}