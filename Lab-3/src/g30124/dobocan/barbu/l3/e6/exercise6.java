package g30124.dobocan.barbu.l3.e6;
import java.util.*;

class MyPoint{
	private int x;
	private int y;
	
	public MyPoint(int newX,int newY)
	{
		x = newX;
		y = newY;
	}
	
	public double distance(int x1,int y1)
	{
		double d;
		d = Math.sqrt((y1-y)*(y1-y) + (x1-x)*(x1-x));
		return d;
	}
	
	public double distance(MyPoint p)
	{
		double d;
		d = Math.sqrt((p.getY()-y)*(p.getY()-y) + (p.getX()-x)*(p.getX()-x));;
		return d;
	}
	
	public void setXY(int newX, int newY)
	{
		x = newX;
		y = newY;
	}
	public int getX()
	{
		return x;
	}
	public int getY()
	{
		return y;
	}
	public String toString()
	{
		return "("+x+","+y+")";
	}
	
	
}
public class exercise6 {
	
	public static void main(String[] args) {	

		MyPoint p1 = new MyPoint(0,0);
		System.out.println(p1.toString());
		int x, y;
		Scanner in = new Scanner(System.in);
		x = in.nextInt();
		y = in.nextInt();
		MyPoint p2 = new MyPoint(x,y);
		System.out.println("Distance bewtween " + p2.toString() + " and " + p1.toString() + " is "+ p1.distance(x,y));
		System.out.println("Distance bewtween " + p2.toString() + " and " + p1.toString() + " is "+ p1.distance(p2));

	}
}
