package g30124.dobocan.barbu.l3.e3;
import becker.robots.*;

public class exercise3 {

	public static void main(String[] args)
	{
		City ny = new City();
		Robot Dave = new Robot(ny, 1, 1, Direction.NORTH);
		Dave.move();
		Dave.move();
		Dave.move();
		Dave.move();
		Dave.move();
		
		Dave.turnLeft();
		Dave.turnLeft();
		
		Dave.move();
		Dave.move();
		Dave.move();
		Dave.move();
		Dave.move();
	}
}
