package g30124.dobocan.barbu.l3.e5;
import becker.robots.*;

public class exercise5 {

	public static void main(String[] args)
	{
	City sm = new City();
	Thing paper = new Thing(sm,2,2);
	Robot Dave = new Robot(sm,1,2,Direction.SOUTH);
	Wall bloc0 = new Wall(sm,1,1,Direction.NORTH);
	Wall bloc1 = new Wall(sm,1,2,Direction.NORTH);
	Wall bloc2 = new Wall(sm,1,2,Direction.EAST);
	//Wall bloc3 = new Wall(sm,2,2,Direction.SOUTH);
	Wall bloc4 = new Wall(sm,1,1,Direction.WEST);
	Wall bloc5 = new Wall(sm,2,1,Direction.WEST);
	Wall bloc6 = new Wall(sm,2,1,Direction.SOUTH);
	Wall bloc7 = new Wall(sm,2,2,Direction.NORTH);
	Dave.turnLeft();
	Dave.turnLeft();
	Dave.turnLeft();
	Dave.move();
	Dave.turnLeft();
	Dave.move();
	Dave.turnLeft();
	Dave.move();
	Dave.pickThing();
	Dave.turnLeft();
	Dave.turnLeft();
	Dave.move();
	Dave.turnLeft();
	Dave.turnLeft();
	Dave.turnLeft();
	Dave.move();
	Dave.turnLeft();
	Dave.turnLeft();
	Dave.turnLeft();
	Dave.move();
	Dave.turnLeft();
	Dave.turnLeft();
	Dave.turnLeft();
	
	}
}
